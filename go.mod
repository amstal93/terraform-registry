module gitlab.dcas.dev/open-source/tf-registry

go 1.15

require (
	github.com/aws/aws-sdk-go v1.35.31
	github.com/djcass44/go-tracer v0.2.0
	github.com/djcass44/go-utils v0.1.3-0.20201113102115-5142d9df90f1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/terraform v0.13.5
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a
	github.com/namsral/flag v1.7.4-pre
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.7
)
