package api

import (
	"github.com/djcass44/go-utils/pkg/httputils"
	"net/http"
)

type ServiceDiscovery struct {
	modules   bool
	providers bool
	login     bool
}

func NewServiceDiscovery() *ServiceDiscovery {
	sd := new(ServiceDiscovery)
	sd.modules = false
	sd.login = false
	sd.providers = true

	return sd
}

// https://www.terraform.io/docs/internals/remote-service-discovery.html#discovery-process
func (sd *ServiceDiscovery) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	doc := DiscoveryDocument{
		ProvidersV1: "/v1/providers/",
	}
	httputils.ReturnJSON(w, http.StatusOK, &doc)
}
