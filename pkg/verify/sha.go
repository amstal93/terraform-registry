package verify

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"strings"
)

func GetSHA(b []byte) (string, error) {
	h := sha256.New()
	if _, err := io.Copy(h, bytes.NewReader(b)); err != nil {
		return "", err
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}

func UpdateChecksumList(src, filename, shasum string) string {
	entries := strings.Split(src, "\n")
	var newEntries []string
	var updated bool
	for _, i := range entries {
		parts := strings.Split(i, "  ")
		if len(parts) != 2 {
			log.Warnf("found invalid line in checksum list, it will be removed: '%s'", i)
			continue
		}
		if parts[1] == filename {
			newEntries = append(newEntries, fmt.Sprintf("%s  %s", shasum, filename))
			updated = true
		} else {
			newEntries = append(newEntries, i)
		}
	}
	if !updated {
		newEntries = append(newEntries, fmt.Sprintf("%s  %s", shasum, filename))
	}
	return strings.Join(newEntries, "\n")
}
