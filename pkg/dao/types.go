package dao

import "gorm.io/gorm"

type ProviderRecord struct {
	gorm.Model
	Namespace    string
	Type         string
	Version      string
	OS           string
	Arch         string
	Name         string
	Filename     string `gorm:"unique"`
	ShaSum       string
	RegistryPath string
}
